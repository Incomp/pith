package me.incomp.pith.util;

import org.bukkit.ChatColor;
import org.bukkit.permissions.Permissible;

/**
 * @author Incomp
 * @since Jul 6, 2016
 */
public enum Perm {
	RENAME("rename", "Allows you to rename your items."),
	RENAME_COLOR("rename.color", "Allows you to rename your items with color codes."),
	RENAME_FORMAT("rename.format", "Allows you to rename your items with formatting codes."),
	
	LORES_ADD("lores.add", "Allows you to add new lines of lore into your items."),
	LORES_REMOVE("lores.remove", "Allows you to remove lines of lore from your items."),
	LORES_SET("lores.set", "Allows you to replace lines of lore on your items."),
	LORES_COLOR("lores.color", "Allows you to use color codes when modifying your items' lore."),
	LORES_FORMAT("lores.format", "Allows you to use formatting codes when modifying your items' lore."),
	
	BASE_INFO("base.info", "Allows you to get basic information about Alloring."),
	BASE_ACCESS("base.access", "Allows you to see what permissions you have.");
	
	private final String key;
	private final String desc;
	private final String node;
	
	private Perm(String key, String desc){
		this.key = key;
		this.desc = desc;
		this.node = "alloring." + key;
	}
	
	public String getKey(){
		return this.key;
	}
	
	public String getDescription(){
		return this.desc;
	}
	
	public String getNode(){
		return this.node;
	}
	
	public String getAccessMessage(Permissible permissible){
		final StringBuilder builder = new StringBuilder();
		if(permissible.hasPermission(this.getNode())){
			builder.append(ChatColor.GREEN + "✔ " + this.getNode());
		}else{
			builder.append(ChatColor.RED + "✘ " + this.getNode());
		}
		
		builder.append(ChatColor.GRAY + " (" + this.getDescription() + ")" + ChatColor.RESET);
		return builder.toString();
	}
	
	public boolean has(Permissible permissible){
		return permissible.hasPermission(this.getNode());
	}
}
