package me.incomp.pith.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.MaterialData;

/**
 * @author Incomp
 * @since Jul 15, 2016
 * 
 * A fancy and chainable util that allows you to create ItemStacks. Created by a fan of the
 * StringBuilder class for other fans of the StringBuilder class.
 */
public class ItemBuilder {
	private final ItemStack item;
	
	/**
	 * Constructs a brand-spankin' new ItemBuilder object. Uses {@link Material#STONE} for
	 * the base ItemStack with an amount of 1.
	 */
	public ItemBuilder(){
		this.item = new ItemStack(Material.STONE);
	}
	
	/**
	 * Constructs a brand-spankin' new ItemBuilder object. Uses a specified Material and
	 * a huge amount of 1 for the base ItemStack.
	 * 
	 * @param m -	Material to use for base ItemStack
	 */
	public ItemBuilder(Material m){
		this.item = new ItemStack(m);
	}
	
	/**
	 * Constructs a brand-spankin' new ItemBuilder. Uses a specified ItemStack as the
	 * base.
	 * 
	 * @param i -	Base ItemStack
	 */
	public ItemBuilder(ItemStack i){
		this.item = i;
	}
	
	/**
	 * Sets the amount of the work-in-progress ItemStack.
	 * 
	 * @param i -	New amount of items in the ItemStack
	 * @return ItemBuilder
	 */
	public ItemBuilder setAmount(int i){
		this.item.setAmount(i);
		return this;
	}
	
	/**
	 * Sets the Material of the work-in-progress ItemStack.
	 * 
	 * @param m
	 * @return ItemBuilder
	 */
	public ItemBuilder setMaterial(Material m){
		this.item.setType(m);
		return this;
	}
	
	/**
	 * Sets the durability of the work-in-progress ItemStack.
	 * 
	 * @param s -	Durability to set
	 * @return 		ItemBuilder
	 */
	public ItemBuilder setDurability(short s){
		this.item.setDurability(s);
		return this;
	}
	
	/**
	 * Sets the name of the work-in-progress ItemStack.
	 * 
	 * @param s -	New name to set
	 * @return 		ItemBuilder
	 */
	public ItemBuilder setName(String s){
		ItemMeta meta = this.item.getItemMeta();
		meta.setDisplayName(this.color(s));
		return this.setItemMeta(meta);
	}
	
	/**
	 * Adds some lines of lore to the work-in-progress ItemStack.
	 * 
	 * @param lore -	Lines of lore to add
	 * @return			ItemBuilder
	 */
	public ItemBuilder addLore(String... lore){
		ItemMeta meta = this.item.getItemMeta();
		List<String> curLore = new ArrayList<>();
		
		// Set current lore to the item's current lore (if it has any)
		if(meta.hasLore()){
			curLore = meta.getLore();
		}
		
		// Add all of the provided lore
		for(String s : lore){
			curLore.add(this.color(s));
		}
		
		meta.setLore(curLore);
		return this.setItemMeta(meta);
	}
	
	/**
	 * Replaces the lore of the work-in-progress ItemStack with a specified list of Strings.
	 * 
	 * @param lore -	New lore to set
	 * @return 			ItemBuilder
	 */
	public ItemBuilder setLore(List<String> lore){
		ItemMeta meta = this.item.getItemMeta();
		List<String> newLore = new ArrayList<>();
		for(String s : lore){
			newLore.add(this.color(s));
		}
		meta.setLore(newLore);
		return this.setItemMeta(meta);
	}
	
	/**
	 * Replaces the lore of the work-in-progress ItemStack with a specified array of Strings.
	 * 
	 * @param lore -	New lore to set
	 * @return 			ItemBuilder
	 */
	public ItemBuilder setLore(String... lore){
		ItemMeta meta = this.item.getItemMeta();
		List<String> newLore = new ArrayList<>();
		for(String s : lore){
			newLore.add(this.color(s));
		}
		meta.setLore(newLore);
		return this.setItemMeta(meta);
	}
	
	/**
	 * Replaces the lore of the work-in-progress ItemStack by splitting a single String into multiple Strings where a specified threshold
	 * is reached.
	 * 
	 * @param s -			Input String
	 * @param lineLength -	Cutoff length
	 * @return 				ItemBuilder
	 */
	public ItemBuilder setLore(String s, int lineLength){
		return this.setLore(this.splitToList(s, lineLength));
	}
	
	/**
	 * Replaces the lore of the work-in-progress ItemStack by splitting a single String into multiple Strings where the 32-character threshold
	 * is reached.
	 * 
	 * @param s -		Input String
	 * @return 			ItemBuilder
	 */
	public ItemBuilder setLore(String s){
		return this.setLore(this.splitToList(s, 32));
	}
	
	/**
	 * Clears all lore.
	 * 
	 * @return ItemBuilder
	 */
	public ItemBuilder clearLore(){
		ItemMeta meta = this.item.getItemMeta();
		meta.setLore(new ArrayList<String>());
		return this.setItemMeta(meta);
	}
	
	/**
	 * Adds all specified ItemFlags to the work-in-progress ItemStack.
	 * 
	 * @param flags - 	ItemFlags to add
	 * @return 			ItemBuilder
	 */
	public ItemBuilder addItemFlags(ItemFlag... flags){
		ItemMeta meta = this.item.getItemMeta();
		meta.addItemFlags(flags);
		return this.setItemMeta(meta);
	}
	
	/**
	 * Removes all specified ItemFlags from the work-in-progress ItemStack.
	 * 
	 * @param flags -	ItemFlags to remove
	 * @return 			ItemBuilder
	 */
	public ItemBuilder removeItemFlags(ItemFlag...flags){
		ItemMeta meta = this.item.getItemMeta();
		meta.removeItemFlags(flags);
		return this.setItemMeta(meta);
	}
	
	/**
	 * Clears all of the ItemFlags from the work-in-progress ItemStack.
	 * 
	 * @return ItemBuilder
	 */
	public ItemBuilder clearItemFlags(){
		ItemMeta meta = this.item.getItemMeta();
		for(ItemFlag flag : meta.getItemFlags()){
			meta.removeItemFlags(flag);
		}
		return this.setItemMeta(meta);
	}
	
	/**
	 * Adds an Enchantment with a specified level to the work-in-progress ItemStack.</br>
	 * If the Enchantment is already present on the item, its level will be overriden.
	 * 
	 * @param ench -	Enchantment to add
	 * @param level -	Enchantment level
	 * @return 			ItemBuilder
	 */
	public ItemBuilder addEnchantment(Enchantment ench, int level){
		if(!this.item.containsEnchantment(ench)){
			this.item.addUnsafeEnchantment(ench, level);
		}else{
			if(this.item.getEnchantmentLevel(ench) != level){
				Map<Enchantment, Integer> enchants = this.item.getEnchantments();
				
				// Clear current enchantments.
				for(Enchantment e : enchants.keySet()){
					this.item.removeEnchantment(e);
				}
				
				enchants.put(ench, level);
				this.item.addEnchantments(enchants);
			}
		}
		return this;
	}
		
	/**
	 * Adds a set of Enchantments to the work-in-progress ItemStack.
	 * 
	 * @param enchants -	Enchantments to add, along with their specified levels
	 * @return 				ItemBuilder
	 */
	public ItemBuilder addEnchantments(Map<Enchantment, Integer> enchants){
		this.item.addUnsafeEnchantments(enchants);
		return this;
	}
		
	/**
	 * Removes a specified Enchantment, or a bunch of them, to the work-in-progress ItemStack.
	 * 
	 * @param enchants - 	Enchantment(s) to remove
	 * @return 				ItemBuilder
	 */
	public ItemBuilder removeEnchantments(Enchantment... enchants){
		for(Enchantment ench : enchants){
			if(this.item.containsEnchantment(ench)){
				this.item.removeEnchantment(ench);
			}else continue;
		}
		return this;
	}
	
	/**
	 * Clears all of the Enchantments from the work-in-progress ItemStack.
	 * 
	 * @return ItemBuilder
	 */
	public ItemBuilder clearEnchantments(){
		Map<Enchantment, Integer> enchants = this.item.getEnchantments();
		for(Enchantment e : enchants.keySet()){
			this.item.removeEnchantment(e);
		}
		return this;
	}
	
	/**
	 * Adds a stored enchantment with a specified level to the work-in-progress ItemStack.</br>
	 * If the ItemStack already has said enchant, it will be overridden.</br>
	 * <br>
	 * <font color=red><b>This won't work if the ItemBuilder's ItemStack is not an enchanted book.</b></font>
	 * 
	 * @param ench -	Enchantment to add
	 * @param level -	Enchantment level
	 * @return 			ItemBuilder
	 */
	public ItemBuilder addStoredEnchantment(Enchantment ench, int level){
		if(this.item.getType() != Material.ENCHANTED_BOOK){
			Bukkit.getLogger().info("Cannot edit the stored enchantments of an item that is not an enchanted book.");
			return this;
		}
		
		EnchantmentStorageMeta esm = (EnchantmentStorageMeta) this.item.getItemMeta();
		if(!esm.getStoredEnchants().containsKey(ench)){
			esm.addStoredEnchant(ench, level, true);
		}else{
			Map<Enchantment, Integer> stored = esm.getStoredEnchants();
			esm.getStoredEnchants().clear();
			stored.put(ench, level);
			for(Entry<Enchantment, Integer> entry : stored.entrySet()){
				esm.addStoredEnchant(ench, level, true);
			}
		}
		return this.setItemMeta(esm);
	}
	
	/**
	 * Removes a specified stored enchantment from the work-in-progress ItemStack.</br>
	 * </br>
	 * <font color=red><b>This won't work if the ItemBuilder's ItemStack is not an enchanted book.</b></font>
	 * 
	 * @param ench -	Enchantment to remove from storage
	 * @return 			ItemBuilder
	 */
	public ItemBuilder removeStoredEnchantment(Enchantment ench){
		if(this.item.getType() != Material.ENCHANTED_BOOK){
			Bukkit.getLogger().info("Cannot edit the stored enchantments of an item that is not an enchanted book.");
			return this;
		}
		
		EnchantmentStorageMeta esm = (EnchantmentStorageMeta) this.item.getItemMeta();
		esm.removeStoredEnchant(ench);
		return this.setItemMeta(esm);
	}
	
	/**
	 * Clears all of the stored enchantments on the work-in-progress ItemStack.</br>
	 * </br>
	 * <font color=red><b>This won't work if the ItemBuilder's ItemStack is not an enchanted book.</b></font>
	 * 
	 * @return ItemBuilder
	 */
	public ItemBuilder clearStoredEnchantments(){
		if(this.item.getType() != Material.ENCHANTED_BOOK){
			Bukkit.getLogger().info("Cannot edit the stored enchantments of an item that is not an enchanted book.");
			return this;
		}
		
		EnchantmentStorageMeta esm = (EnchantmentStorageMeta) this.item.getItemMeta();
		esm.getStoredEnchants().clear();
		return this;
	}
	
	/**
	 * Sets the owner of the work-in-progress ItemStack, given that it's a player head.</br>
	 * </br>
	 * <font color=red><b>This won't work if the ItemBuilder's ItemStack is not a player head.</b></font>
	 * 
	 * @param s - 	Username of the owner to set
	 * @return 		ItemBuilder
	 */
	public ItemBuilder setOwner(String s){
		if(this.item.getType() != Material.SKULL_ITEM){
			Bukkit.getLogger().info("Cannot edit the owner of an item that is not a player skull.");
			return this;
		}
		
		if(this.item.getDurability() != 3){
			Bukkit.getLogger().info("Cannot edit the owner of an item that is not a player skull.");
			return this;
		}
		
		SkullMeta sm = (SkullMeta) this.item.getItemMeta();
		sm.setOwner(s);
		return this.setItemMeta(sm);
	}
	
	/**
	 * Sets the ItemMeta of the work-in-progress ItemStack.
	 * 
	 * @param im -	ItemMeta to set
	 * @return 		ItemBuilder
	 */
	public ItemBuilder setItemMeta(ItemMeta im){
		this.item.setItemMeta(im);
		return this;
	}
	
	/**
	 * Sets the MaterialData of the work-in-progress ItemStack.
	 * 
	 * @param md -	MaterialData to set
	 * @return 		ItemBuilder
	 */
	public ItemBuilder setMaterialData(MaterialData md){
		this.item.setData(md);
		return this;
	}
	
	/**
	 * Copies over this ItemBuilder so you can branch off any make a mutated version. Awesome.
	 */
	public ItemBuilder clone(){
		return new ItemBuilder(this.item);
	}
	
	/**
	 * Gets the current ItemStack.
	 * 
	 * @return ItemStack
	 */
	public ItemStack toItemStack(){
		return this.item;
	}
	
	/**
	 * Colors a String with fancy color codes.
	 * 
	 * @param s -		Input String to colorize
	 * @return String
	 */
	private String color(String s){
		return ChatColor.translateAlternateColorCodes('&', s);
	}
	
	/**
	 * Takes a String and splits it into a list of Strings, dictated by a
	 * cutoff length.
	 * 
	 * @param input -	Input String to split	
	 * @param cutoff -	Cutoff length to split at
	 * @return 			List<String>
	 */
	private List<String> splitToList(String input, int cutoff){
		List<String> out = new ArrayList<String>();
		Pattern pat = Pattern.compile("\\G\\s*(.{1," + cutoff + "})(?=\\s|$)", Pattern.DOTALL);
		Matcher m = pat.matcher(input);
		while(m.find()){
			out.add(m.group(1));
		}
		return out;
	}
}