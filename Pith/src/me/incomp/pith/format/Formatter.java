package me.incomp.pith.format;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * @author Incomp</br>
 * @since Jul 24, 2016</br>
 * </br>
 * This class is designed to simplify the process of creating presentable Strings
 * to send to whatever you want; Players or otherwise. Each method should have
 * elaborate documentation.</br>
 * </br>
 * No need for me to paste in some licensing jargon. You are more than welcome to
 * use and modify this for yourself. However, I'd like for my name to appear in
 * the credits. Can't let you forget how awesome I am.</br>
 * </br>
 * With that being said, enjoy programming.
 */
public class Formatter {
	private Formatter(){}; // No accidental construction
	
	public static final String DEBUG_PERM = "pith.debug";
	public static final String MAIN_PACKAGE = "me.incomp.pith";
	
	public static String color(char altColorChar, String input){
		return ChatColor.translateAlternateColorCodes(altColorChar, input);
	}
	
	public static String color(String input){
		return Formatter.color('&', input);
	}
	
	public static String stripColor(String input){
		return ChatColor.stripColor(input);
	}
	
	public static String format(String header, String body, FormatType type){
		return type.getChatColor() + color(header) + ChatColor.BOLD + " > " + ChatColor.GRAY + color(body) + ChatColor.RESET;
	}
	
	public static void log(String input, Level logLevel){
		Bukkit.getLogger().log(logLevel, Formatter.color(input));
	}
		
	public static String getLineRule(ChatColor color){
		return color + "" + ChatColor.STRIKETHROUGH + "-----------------------------------------------------";
	}
		
	public static String getLineRule(){
		return Formatter.getLineRule(ChatColor.DARK_GRAY);
	}
	
	public static void broadcast(String input){
		for(Player p : Bukkit.getOnlinePlayers()){
			p.sendMessage(color(input));
		}
	}
	
	public static void broadcast(List<String> input){
		for(Player p : Bukkit.getOnlinePlayers()){
			for(String s : input){
				p.sendMessage(color(s));
			}
		}
	}
	
	public static void broadcast(String input, String perm){
		for(Player p : Bukkit.getOnlinePlayers()){
			if(p.hasPermission(perm)){
				p.sendMessage(color(input));
			}
		}
	}
	
	public static void broadcast(List<String> input, String perm){
		for(Player p : Bukkit.getOnlinePlayers()){
			if(p.hasPermission(perm)){
				for(String s : input){
					p.sendMessage(color(s));
				}
			}else continue;
		}
	}
	
	public static List<String> wrap(String wrapper, int thickness, String... enclosed){
		return Formatter.wrap(wrapper, thickness, Arrays.asList(enclosed));
	}
	
	public static List<String> wrap(String wrapper, int thickness, List<String> enclosed){
		List<String> list = new ArrayList<>();
		for(int i = 0; i < thickness; i++){
			list.add(color(wrapper));
		}
		for(String s : enclosed){
			list.add(color(s));
		}
		for(int i = 0; i < thickness; i++){
			list.add(color(wrapper));
		}
		return list;
	}
	
	public static void clear(Player p, int amount){
		if(amount <= 0) return;
		for(int i = 0; i < amount; i++){
			p.sendMessage("");
		}
	}
	
	public static void clear(Player p){
		Formatter.clear(p, 128);
	}
	
	public static void clear(int amount){
		if(amount <= 0) return;
		for(int i = 0; i < amount; i++){
			Formatter.broadcast("");
		}
	}
	
	public static void clear(){
		Formatter.clear(128);
	}
	
	public static void debug(String msg){
		final String s = Formatter.color(msg);
		for(Player p : Bukkit.getOnlinePlayers()){
			if(p.hasPermission(DEBUG_PERM)) p.sendMessage(Formatter.format("Debug", msg, FormatType.ERROR));
		}
		System.out.println(Formatter.stripColor(s));
	}

	public static void debug(Player p, String msg){
		final String s = Formatter.color(msg);
		p.sendMessage(Formatter.format("Debug", s, FormatType.ERROR));
		System.out.println(Formatter.stripColor(s));
	}
	
	public static void sendExceptionDebug(Throwable ex){
		if(ex instanceof InvocationTargetException){ // Invocation can be multiple exceptions, this will grab the real one.
			ex = ((InvocationTargetException) ex).getTargetException();
		}
		StringBuilder sb = new StringBuilder("");
		sb.append(ex.getClass().getName() + ",");
		boolean relevant = false; // Checks if the error is relevant to this plugin. Maybe have a whitelist feature that allows for multiple class name checks.
		for(StackTraceElement ste : ex.getStackTrace()){
			String[] info = ste.getClassName().split("\\.");
			String clazz = info[info.length - 1];
			if(ste.getClassName().startsWith(MAIN_PACKAGE)){
				relevant = true;
				sb.append("at " + clazz + "." + ste.getMethodName() + "(" + ste.getLineNumber() + "),");
			}
		}
		if(!relevant) return;
		for(Player p : Bukkit.getOnlinePlayers()){
			if(p.hasPermission(DEBUG_PERM)){
				for(String str : sb.toString().split(",")){
					if(str.length() > 0){
						debug(p, str);
					}
				}
			}
		}
	}
	
	public static List<String> splitToList(String input, int length){
		List<String> out = new ArrayList<String>();
		Pattern pat = Pattern.compile("\\G\\s*(.{1," + length + "})(?=\\s|$)", Pattern.DOTALL);
		Matcher m = pat.matcher(input);
		while(m.find()){
			out.add(m.group(1));
		}
		return out;
	}
	
	public static List<String> splitToList(String input, int length, ChatColor color){
		List<String> out = new ArrayList<String>();
		input = Formatter.color(input);
		Pattern pat = Pattern.compile("\\G\\s*(.{1," + length + "})(?=\\s|$)", Pattern.DOTALL);
		Matcher m = pat.matcher(input);
		while(m.find()){
			out.add(color + color(m.group(1)));
		}
		return out;
	}
}