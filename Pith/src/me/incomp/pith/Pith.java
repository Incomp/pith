package me.incomp.pith;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import me.incomp.pith.format.FormatType;
import me.incomp.pith.format.Formatter;
import me.incomp.pith.sk89qcmd.CommandException;
import me.incomp.pith.sk89qcmd.CommandPermissionsException;
import me.incomp.pith.sk89qcmd.CommandUsageException;
import me.incomp.pith.sk89qcmd.CommandsManager;
import me.incomp.pith.sk89qcmd.MissingNestedCommandException;
import me.incomp.pith.sk89qcmd.WrappedCommandException;
import me.incomp.pith.sk89qcmd.bukkit.CommandsManagerRegistration;

/**
 * The primary class for Pith, a Spigot plugin.
 *
 * @author Incomp
 * @since Nov 8, 2016
 */
public class Pith extends JavaPlugin {
	
	private static Pith INSTANCE;
	
	// sk89q's command framework
	private CommandsManager<CommandSender> commands;
	private CommandsManagerRegistration cmdReg;
	
	public void onEnable(){
		INSTANCE = this;
		this.commands = new CommandsManager<CommandSender>() {
			@Override
			public boolean hasPermission(CommandSender sender, String perm) {
				return sender instanceof ConsoleCommandSender || sender.hasPermission(perm);
			}
		};
		this.cmdReg = new CommandsManagerRegistration(this, this.commands);
		this.setupCommands();
		// TODO: This whole method
	}
	
	public void onDisable(){
		// TODO: This whole method
		HandlerList.unregisterAll(this);
		this.cmdReg.unregisterCommands();
		INSTANCE = null;
	}
	
	public static Pith getInstance(){
		return INSTANCE;
	}
	
	private void registerEvents(Listener... listeners){
		for(Listener li : listeners){
			Bukkit.getPluginManager().registerEvents(li, this);
		}
	}
	
	/**
	 * This just catches each command and tries to execute it if possible. Don't call it.
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		try {
			this.commands.execute(cmd.getName(), args, sender, sender);
		} catch (CommandPermissionsException e) {
			sender.sendMessage(Formatter.format("Access", "Access denied.", FormatType.ERROR));
		} catch (MissingNestedCommandException e) {
			sender.sendMessage(Formatter.format("Syntax", "You're missing a nested command. Try this:", FormatType.ERROR));
			sender.sendMessage(Formatter.format("Syntax", e.getUsage(), FormatType.ERROR));
		} catch (CommandUsageException e) {
			sender.sendMessage(Formatter.format("Syntax", e.getMessage() + " Try this:", FormatType.ERROR));
			sender.sendMessage(Formatter.format("Syntax", e.getUsage(), FormatType.ERROR));
		} catch (WrappedCommandException e) {
			if (e.getCause() instanceof NumberFormatException) {
				sender.sendMessage(Formatter.format("Syntax", "We needed a number, but you gave us a string!", FormatType.ERROR));
			} else {
				sender.sendMessage(Formatter.format("OH NO", "The end is upon us! Contact NobleProductions, Lew_, or Incomp immediately.", FormatType.ERROR));
				e.printStackTrace();
			}
		} catch (CommandException e) {
			sender.sendMessage(Formatter.format("Syntax", e.getMessage(), FormatType.ERROR));
		}
		return true;
	}

	
	/**
	 * Registers the commands used in the plugin.
	 * To add a command, call: {@link CommandsManagerRegistration#register(Class)}
	 * 
	 * @see http://bukkit.org/threads/tut-using-sk89qs-command-framework.185423/
	 */
	private void setupCommands() {
		// this.cmdReg.register(SomeCommand.class)
	}
}