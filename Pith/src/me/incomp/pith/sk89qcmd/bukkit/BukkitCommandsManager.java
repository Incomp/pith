package me.incomp.pith.sk89qcmd.bukkit;

import org.bukkit.command.CommandSender;

import me.incomp.pith.sk89qcmd.CommandsManager;

public class BukkitCommandsManager extends CommandsManager<CommandSender> {
    @Override
    public boolean hasPermission(CommandSender player, String perm) {
        return player.hasPermission(perm);
    }
}
